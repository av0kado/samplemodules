import UIKit

/// Item for CommonTabBar module
enum CommonTabBarItem {
    typealias TabBarItemConfiguration = (image: UIImage, selectedImage: UIImage, title: String)
    
    case feed(Account?)
    case news(Account?)
    case posts(Account?)
    case profile(Account?)
    
    func configuration() -> TabBarItemConfiguration {
        
        var image: UIImage
        var selectedImage: UIImage
        var title: String
        
        switch self {
        case .news(_):
            image = #imageLiteral(resourceName: "TabBarFeed").withRenderingMode(.alwaysOriginal)
            selectedImage = #imageLiteral(resourceName: "TabBarFeedSelected").withRenderingMode(.alwaysOriginal)
            title = "news"
        case .feed(_):
            image = #imageLiteral(resourceName: "TabBarFeed").withRenderingMode(.alwaysOriginal)
            selectedImage = #imageLiteral(resourceName: "TabBarFeedSelected").withRenderingMode(.alwaysOriginal)
            title = "feed"
        case .posts(_):
            image = #imageLiteral(resourceName: "TabBarFeed").withRenderingMode(.alwaysOriginal)
            selectedImage = #imageLiteral(resourceName: "TabBarFeedSelected").withRenderingMode(.alwaysOriginal)
            title = "posts"
        case .profile(_):
            image = #imageLiteral(resourceName: "TabBarFeed").withRenderingMode(.alwaysOriginal)
            selectedImage = #imageLiteral(resourceName: "TabBarFeedSelected").withRenderingMode(.alwaysOriginal)
            title = "profile"
        }
        
        return (image, selectedImage, title)
    }
    
    /// Returns standard array of items for input account
    ///
    /// - Parameter account: account to customize tabbar
    /// - Returns: array of `CommonTabBarItem` elements
    static func standardItems(for account: Account?) -> [CommonTabBarItem] {
        guard let account = account, let accountType = account.accountType, account.id != nil else {
            return [.news(nil), .feed(nil), .posts(nil), .profile(nil)]
        }
        
        var items: [CommonTabBarItem]
        
        switch accountType {
        case .typical:
            items = [.feed(nil), .news(nil), .posts(nil), .profile(nil)]
        case .other:
            items = [.posts(nil), .news(nil), .feed(nil), .profile(nil)]
        }
        
        return items
    }
}
