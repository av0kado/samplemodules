import UIKit

class CommonTabBarModuleLoader: BaseModuleLoader {
    
    lazy var storyboardsFactory: StoryboardsFactory = StoryboardsFactoryImplementation()

    override func configureModule(for view: UIViewController) {

        let viewController = view as! CommonTabBarViewController

        let router = CommonTabBarRouter()
        router.presentationView = view  as! UITabBarController
        router.showsListModuleLoader = BaseModuleLoader()
        router.fanProfileModuleLoader = BaseModuleLoader()
        router.eventsModuleLoader = BaseModuleLoader()

        let presenter = CommonTabBarPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = CommonTabBarInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

    override func loadModuleViewController() -> UIViewController {
        let controller = storyboardsFactory.getStoryboard(with: .commonTabBar).instantiateViewController(with: .tabBarController)
        return controller
    }

}
