protocol CommonTabBarRouterInput {
    
    /// Present tab bar tabs with provided configuration
    ///
    /// - Parameter configuration: configuration of tab bar items
    func presentTabs(with configuration: CommonTabBarModuleConfiguration)
}
