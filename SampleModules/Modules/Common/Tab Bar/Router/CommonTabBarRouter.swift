import UIKit

class CommonTabBarRouter: CommonTabBarRouterInput {

    weak var presentationView: UITabBarController!
    var showsListModuleLoader: BaseModuleLoader!
    var fanProfileModuleLoader: BaseModuleLoader!
    var eventsModuleLoader: BaseModuleLoader!

    // MARK: - CommonTabBarRouterInput
    
    func presentTabs(with configuration: CommonTabBarModuleConfiguration) {
        
        var controllers = [UIViewController]()
        for item in configuration.items {
            let controller = loadAndConfigureController(for: item)
            controllers.append(controller)
        }
        
        presentationView.viewControllers = controllers
        presentationView.selectedIndex = configuration.selectedItem
    }
    
    // MARK: - Private
    
    func loadAndConfigureController(for tabBarItem: CommonTabBarItem) -> UIViewController {
        
        var controller: UIViewController
        var controllerConfiguration: Any?
        
        // Load controller and configuration
        switch tabBarItem {
        case .news(let account):
            controller = UIViewController()
            controllerConfiguration = account
        case .feed(let account):
            controller = UIViewController()
            controllerConfiguration = account
        case .posts(let account):
            controller = UIViewController()
            controllerConfiguration = account
        case .profile(let account):
            controller = UIViewController()
            controllerConfiguration = account
        }
        
        // Configure tab bar item
        let configuration = tabBarItem.configuration()
        controller.tabBarItem.image = configuration.image
        controller.tabBarItem.selectedImage = configuration.selectedImage
        controller.tabBarItem.title = configuration.title
        
        let font = UIFont.systemFont(ofSize: 11.0)
        controller.tabBarItem.setTitleTextAttributes([.foregroundColor : #colorLiteral(red: 0.7960784314, green: 0.7960784314, blue: 0.7960784314, alpha: 1), .font : font], for: .normal)
        controller.tabBarItem.setTitleTextAttributes([.foregroundColor : #colorLiteral(red: 1, green: 0.1607843137, blue: 0.4039215686, alpha: 1), .font : font], for: .selected)
        
        // Configure controller for a tab
        if let configuration = controllerConfiguration, let configurableController = controller as? ConfigurableModuleController {
            configurableController.configureModule(with: configuration)
        }
        
        // Add navigation controller
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.isNavigationBarHidden = true
        
        // Preload profile
        switch tabBarItem {
        case .profile(_):
            let _ = controller.view
        default:
            break
        }
        
        return navigationController
    }
}
