//
//  CommonTabBarViewInput.swift
//  Mouse
//
//  Created by Amir Zigangarayev on 29/11/2017.
//  Copyright © 2017 Mouse. All rights reserved.
//

protocol CommonTabBarViewInput: class {

    /// Configures initial state of view
    func setupInitialState()
}
