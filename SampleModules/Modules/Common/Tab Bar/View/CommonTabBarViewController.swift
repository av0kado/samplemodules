import UIKit

class CommonTabBarViewController: UITabBarController, CommonTabBarViewInput, ConfigurableModuleController {

    var output: CommonTabBarViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    // MARK: CommonTabBarViewInput

    func setupInitialState() {
        tabBar.barTintColor = .black
    }
    
    // MARK: ConfigurableModuleController
    
    func configureModule(with object: Any) {
        
        if let configuration = object as? CommonTabBarModuleConfiguration {
            output.configure(with: configuration)
        }
    }
}
