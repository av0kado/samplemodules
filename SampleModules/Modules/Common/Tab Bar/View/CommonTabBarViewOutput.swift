//
//  CommonTabBarViewOutput.swift
//  Mouse
//
//  Created by Amir Zigangarayev on 29/11/2017.
//  Copyright © 2017 Mouse. All rights reserved.
//

protocol CommonTabBarViewOutput {

    /// View is loaded and ready to init
    func viewIsReady()
    
    /// Configure module
    func configure(with configuration: CommonTabBarModuleConfiguration)
}
