class CommonTabBarPresenter: CommonTabBarViewOutput, CommonTabBarInteractorOutput {

    weak var view: CommonTabBarViewInput!
    var interactor: CommonTabBarInteractorInput!
    var router: CommonTabBarRouterInput!
    
    // MARK: - CommonTabBarViewOutput
    
    func viewIsReady() {
        view.setupInitialState()
    }

    // MARK: - CommonTabBarInteractorOutput

    func configure(with configuration: CommonTabBarModuleConfiguration) {
        router.presentTabs(with: configuration)
    }
}
