import Photos

/// An entity to encapsulate an asset
struct ImagePickerAssetEntity {
    
    let asset: PHAsset
}
