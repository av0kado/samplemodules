import UIKit

protocol ImagePickerInteractorOutput: class {

    /// Interactor did finish loading images
    ///
    /// - Parameter images: images assets
    func didFinishLoading(images: [ImagePickerAssetEntity])
    
    
    /// Image loaded for selected asset
    ///
    /// - Parameter image: image loaded
    func didFinishLoading(image: UIImage)
}
