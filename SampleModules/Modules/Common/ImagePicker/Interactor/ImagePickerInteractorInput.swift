import Photos

protocol ImagePickerInteractorInput {
    
    /// Load images assets for image picker
    func beginLoadingImages()
    
    /// Load image for selected asset entity
    func loadImage(with assetEntity: ImagePickerAssetEntity)
}
