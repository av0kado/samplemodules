import Photos

class ImagePickerInteractor: ImagePickerInteractorInput {

    weak var output: ImagePickerInteractorOutput!
    
    let imageManager: PHImageManager = PHCachingImageManager.default()

    // MARK: - ImagePickerInteractorInput
    
    func beginLoadingImages() {
        
        let options = PHFetchOptions()
        let sortDescriptor = NSSortDescriptor(key: "creationDate", ascending: false)
        options.sortDescriptors = [sortDescriptor]
        options.predicate = NSPredicate(format: "mediaType = %d", PHAssetMediaType.image.rawValue)
        
        let fetchResult = PHAsset.fetchAssets(with: options)
        
        var entities = [ImagePickerAssetEntity]()
        fetchResult.enumerateObjects { (asset, _, _) in
            
            let entity = ImagePickerAssetEntity(asset: asset)
            entities.append(entity)
        }
        
        output.didFinishLoading(images: entities)
    }
    
    func loadImage(with assetEntity: ImagePickerAssetEntity) {
        
        let options = PHImageRequestOptions()
        options.isSynchronous = true
        
        imageManager.requestImage(for: assetEntity.asset, targetSize: PHImageManagerMaximumSize, contentMode: .default, options: options) { (image, _) in
            
            if let image = image {
                self.output.didFinishLoading(image: image)
            }
        }
    }
}
