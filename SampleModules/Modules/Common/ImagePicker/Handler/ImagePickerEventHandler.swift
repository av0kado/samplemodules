import UIKit

protocol ImagePickerEventHandler: class {
    
    /// Image picker did select image
    ///
    /// - Parameter image: selected image
    func imagePickerDidFinish(with image: UIImage)
    
    /// Image picker did cancel
    func imagePickerDidCancel()
}
