import UIKit

class ImagePickerPresenter: ImagePickerViewOutput, ImagePickerInteractorOutput {

    weak var view: ImagePickerViewInput!
    var interactor: ImagePickerInteractorInput!
    var router: ImagePickerRouterInput!
    var modelsFactory: ImagePickerCellDataSourceFactory!
    
    var selectionItemsById: [String : ImagePickerAssetEntity]!
    weak var eventHandler: ImagePickerEventHandler?
    
    // MARK: - ImagePickerViewOutput
    
    func configure(with handler: ImagePickerEventHandler) {
        
        eventHandler = handler
    }
    
    func viewIsReady() {
        view.setupInitialState()
    }
    
    func viewWillPresent() {
        view.prepareAppearance()
        interactor.beginLoadingImages()
    }
    
    func didSelectItem(with objectId: String) {
        
        if let model = selectionItemsById[objectId] {
            
            interactor.loadImage(with: model)
        }
    }
    
    func didCancel() {
        
        eventHandler?.imagePickerDidCancel()
    }

    // MARK: - ImagePickerInteractorOutput

    func didFinishLoading(images: [ImagePickerAssetEntity]) {
        let imageSize = view.imageSize()
        let configuration = modelsFactory.buildDataSourceConfiguration(with: images, imageSize: imageSize)
        selectionItemsById = configuration.selectionItems
        
        view.displayCollection(with: configuration.dataSource)
    }
    
    func didFinishLoading(image: UIImage) {
        
        eventHandler?.imagePickerDidFinish(with: image)
    }
}
