import UIKit

class ImagePickerModuleLoader: BaseModuleLoader {
    
    lazy var storyboardsFactory: StoryboardsFactory = StoryboardsFactoryImplementation()

    override func configureModule(for view: UIViewController) {

        let viewController = view as! ImagePickerViewController

        let router = ImagePickerRouter()
        router.presentationView = view

        let presenter = ImagePickerPresenter()
        presenter.modelsFactory = ImagePickerCellDataSourceFactoryImplementation()
        presenter.view = viewController
        presenter.router = router

        let interactor = ImagePickerInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

    override func loadModuleViewController() -> UIViewController {
        
        let controller = storyboardsFactory.getStoryboard(with: .imagePicker).instantiateViewController(with: .imagePickerCollection)
        return controller
    }

}
