import Foundation
import CoreGraphics

class ImagePickerCellDataSourceFactoryImplementation: ImagePickerCellDataSourceFactory {
    
    func buildDataSourceConfiguration(with dataArray: [ImagePickerAssetEntity], imageSize: CGSize) -> ImagePickerCellDataSourceConfiguration {
        
        let dataStructure = CollectionViewDataSourceStructure()
        var selectionItems = [String : ImagePickerAssetEntity]()
        
        var cellObjects = [ImagePickerCellObject]()
        
        for currentModel in dataArray.enumerated() {
            
            let model = currentModel.element
            let cellObject = ImagePickerCellObject(itemId: String(currentModel.offset), assetEntity: model, imageSize: imageSize)
            
            cellObjects.append(cellObject)
            selectionItems[cellObject.itemId] = model
        }
        
        dataStructure.appendSection(with: cellObjects)
        
        let dataSource = CollectionViewDataSource(with: dataStructure)
        
        return (dataSource: dataSource, selectionItems: selectionItems)
    }
}
