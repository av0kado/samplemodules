import Foundation
import CoreGraphics

typealias ImagePickerCellDataSourceConfiguration = (dataSource: CollectionViewDataSource, selectionItems: [String : ImagePickerAssetEntity])

/// Factory for generating CollectionViewDataSource with `ImagePickerCellObejct`'s
protocol ImagePickerCellDataSourceFactory {
    
    func buildDataSourceConfiguration(with dataArray: [ImagePickerAssetEntity], imageSize: CGSize) -> ImagePickerCellDataSourceConfiguration
}
