import Photos

class ImagePickerCellObject: CellObjectWithId {
    
    var itemId: String
    var assetEntity: ImagePickerAssetEntity
    var imageSize: CGSize
    
    init(itemId: String, assetEntity: ImagePickerAssetEntity, imageSize: CGSize) {
        
        self.itemId = itemId
        self.assetEntity = assetEntity
        self.imageSize = imageSize
    }
}
