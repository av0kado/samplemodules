import UIKit
import Photos

class ImagePickerCell: UICollectionViewCell, ConfigurableView {

    var imageManager = PHCachingImageManager.default()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var imageView: UIImageView!
    
    func configure(with object: Any) {
        
        guard let cellObject = object as? ImagePickerCellObject else {
            return
        }
        
        // Configure with cellObject...
        
        let asset = cellObject.assetEntity.asset
        
        imageManager.requestImage(for: asset, targetSize: cellObject.imageSize, contentMode: .aspectFill, options: nil) { (image, _) in
            
            self.imageView.image = image
        }
    }

}
