import CoreGraphics

protocol ImagePickerViewInput: class {

    /// Configures initial state of view
    func setupInitialState()
    
    /// Tells view to prepare it's appearance
    func prepareAppearance()
    
    /// Tells view to display collection with `CollectionViewDataSource`
    ///
    /// - Parameter dataSource: CollectionViewDataSource object
    func displayCollection(with dataSource: CollectionViewDataSource)
    
    /// Get image size to be available to configure ImagePickerCellDataSource
    ///
    /// - Returns: CGSize of image previewed
    func imageSize() -> CGSize
}
