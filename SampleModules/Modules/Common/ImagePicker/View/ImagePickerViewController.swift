import UIKit

class ImagePickerViewController: UIViewController, ImagePickerViewInput, UICollectionViewDelegateFlowLayout, ConfigurableModuleController {

    var output: ImagePickerViewOutput!
    var collectionViewDataDisplayManager: CollectionViewDataSource!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    var cellSize: CGSize {
        get {
            if let cellSize = _cellSize {
                return cellSize
            }
            
            let collectionWidth = collectionView.bounds.width
            let cellWidth = (collectionWidth - 30 - 46) / 3
            _cellSize = CGSize(width: cellWidth, height: cellWidth)
            
            return _cellSize!
        }
    }
    
    private var _cellSize: CGSize?
    
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        output.viewWillPresent()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .default
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // MARK: - Actions
    
    @objc func cancelButtonPressed() {
        output.didCancel()
    }
    
    // MARK: - ImagePickerViewInput

    func setupInitialState() {
        
        collectionView.registerCellNib(for: ImagePickerCellObject.self)
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
    }
    
    func prepareAppearance() {
        
        _cellSize = nil
    }
    
    func displayCollection(with dataSource: CollectionViewDataSource) {
        
        collectionViewDataDisplayManager = dataSource
        collectionViewDataDisplayManager.delegate = self
        collectionViewDataDisplayManager.assign(to: collectionView)
        
        collectionView.reloadData()
    }
    
    func imageSize() -> CGSize {
        
        return cellSize
    }
    
    // MARK: - UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return cellSize
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let dataStructure = collectionViewDataDisplayManager.dataStructure
        guard let cellObject = dataStructure.cellObject(at: indexPath) as? CellObjectWithId else {
            return
        }
        
        output.didSelectItem(with: cellObject.itemId)
    }
    
    // MARK: - ConfigurableModuleController
    
    func configureModule(with object: Any) {
        
        if let eventHandler = object as? ImagePickerEventHandler {
            output.configure(with: eventHandler)
        }
    }
}
