protocol ImagePickerViewOutput {

    /// View is loaded and ready to init
    func viewIsReady()
    
    /// View is about to present
    func viewWillPresent()
    
    /// Configure module
    func configure(with handler: ImagePickerEventHandler)
    
    /// Collection CellObjectWithId selected
    ///
    /// - Parameter objectId: id of selected item
    func didSelectItem(with objectId: String)
    
    /// View `Cancel` pressed
    func didCancel()
}
