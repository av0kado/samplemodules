import UIKit

class SignUpEnterCodeViewController: UIViewController, SignUpEnterCodeViewInput, ConfigurableModuleController, CodeInputViewDelegate {

    var output: SignUpEnterCodeViewOutput!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var codeInputView: CodeInputView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    // MARK: ConfigurableModuleController
    
    func configureModule(with object: Any) {
        guard let phoneNumber = object as? String else {
            return
        }
        output.configure(with: phoneNumber)
    }
    
    // MARK: - Actions
    
    @objc func cancelButtonPressed() {
        output.cancelPressed()
    }
    
    @objc func skipButtonPressed() {
        output.skipPressed()
    }
    
    @objc func resendButtonPressed() {
        output.resendPressed()
    }
    
    // MARK: - SignUpEnterCodeViewInput
    
    func setupInitialState() {
        codeInputView.becomeFirstResponder()
        codeInputView.delegate = self
        
        cancelButton.addTarget(self, action: #selector(cancelButtonPressed), for: .touchUpInside)
        skipButton.addTarget(self, action: #selector(skipButtonPressed), for: .touchUpInside)
        resendButton.addTarget(self, action: #selector(resendButtonPressed), for: .touchUpInside)
    }
    
    func setPhoneText(_ phoneText: String) {
        textLabel.text = phoneText
    }
    
    // MARK: - CodeInputViewDelegate
    
    func codeInputView(_ view: CodeInputView, didEnter code: String) {
        if code.count == 4 {
            output.codeEntered(code: code)
        }
    }
}
