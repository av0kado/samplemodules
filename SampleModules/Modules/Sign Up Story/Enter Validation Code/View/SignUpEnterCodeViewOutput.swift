protocol SignUpEnterCodeViewOutput {

    /// Configure module with given phone number
    func configure(with phoneNumber: String)
    
    /// View is loaded and ready to init
    func viewIsReady()
    
    /// Cancel button pressed
    func cancelPressed()
    
    /// Skip button pressed
    func skipPressed()
    
    /// Resend code button pressed
    func resendPressed()
    
    /// View did enter code
    func codeEntered(code: String)
}
