protocol SignUpEnterCodeViewInput: class {

    /// Configures initial state of view
    func setupInitialState()
    
    /// Set phone text displayed
    func setPhoneText(_ phoneText: String)
}
