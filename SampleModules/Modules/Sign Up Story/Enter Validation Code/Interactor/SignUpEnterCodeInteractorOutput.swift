protocol SignUpEnterCodeInteractorOutput: class {

    /// Interactor did finish validating code
    ///
    /// - Parameter result: result of request
    func didFinishCodeValidation(with result: PhoneValidationServiceResult)
}
