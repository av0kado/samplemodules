import Foundation

class SignUpEnterCodeInteractor: SignUpEnterCodeInteractorInput {

    weak var output: SignUpEnterCodeInteractorOutput!
    var phoneValidationService: PhoneValidationService!
    var currentTask: URLSessionTask?

    func validate(code: String, for phoneNumber: String) {
        currentTask?.cancel()
        
        currentTask = phoneValidationService.validatePhoneNumber(phoneNumber, with: code, completionHandler: { (result) in
            self.output?.didFinishCodeValidation(with: result)
        })
    }
}
