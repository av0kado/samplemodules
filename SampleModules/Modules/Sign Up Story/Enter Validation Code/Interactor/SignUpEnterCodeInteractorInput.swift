import Foundation

protocol SignUpEnterCodeInteractorInput {
    
    /// Validate code for given phone number
    ///
    /// - Parameters:
    ///   - code: entered code
    ///   - phoneNumber: phone number set
    func validate(code: String, for phoneNumber: String)
}
