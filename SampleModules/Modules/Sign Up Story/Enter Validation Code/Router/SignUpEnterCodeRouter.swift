import UIKit

class SignUpEnterCodeRouter: SignUpEnterCodeRouterInput {

    weak var presentationView: UIViewController!
    var alertsFactory: CommonAlertsFactory!
    var createAccountModuleLoader: BaseModuleLoader!
    var tabBarModuleLoader: BaseModuleLoader!
    
    func presentCreateAccountModule(with configuration: Any?) {
        let controller = createAccountModuleLoader.loadModuleViewController()
        
        if let configuration = configuration, let configurableController = controller as? ConfigurableModuleController {
            configurableController.configureModule(with: configuration)
        }
        
        presentationView.navigationController?.pushViewController(controller, animated: true)
    }
    
    func presentAlert(with text: String?) {
        let alertController = alertsFactory.getOKAlert(withTitle: nil, text: text)
        presentationView.present(alertController, animated: true, completion: nil)
    }
    
    func presentPreviousController() {
        presentationView.navigationController?.popViewController(animated: true)
    }
    
    func presentTabBar(with configuration: CommonTabBarModuleConfiguration) {
        let controller = tabBarModuleLoader.loadModuleViewController()
        
        if let configurableController = controller as? ConfigurableModuleController {
            configurableController.configureModule(with: configuration)
        }
        
        UIApplication.appDelegate.setNewRootViewController(controller)
    }
}
