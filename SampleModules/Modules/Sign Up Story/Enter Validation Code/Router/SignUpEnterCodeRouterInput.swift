//
//  SignUpEnterCodeRouterInput.swift
//  Mouse
//
//  Created by Amir Zigangarayev on 15/11/2017.
//  Copyright © 2017 Mouse. All rights reserved.
//

import Foundation

protocol SignUpEnterCodeRouterInput {

    /// Presents alert with given text
    func presentAlert(with text: String?)
    
    /// Present createAccountModule
    func presentCreateAccountModule(with configuration: Any?)
    
    /// Drives back
    func presentPreviousController()
    
    /// Presents tabbar module with provided configuration
    func presentTabBar(with configuration: CommonTabBarModuleConfiguration)
}
