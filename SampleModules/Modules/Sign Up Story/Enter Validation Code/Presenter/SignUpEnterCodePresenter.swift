class SignUpEnterCodePresenter: SignUpEnterCodeViewOutput, SignUpEnterCodeInteractorOutput {

    weak var view: SignUpEnterCodeViewInput!
    var interactor: SignUpEnterCodeInteractorInput!
    var router: SignUpEnterCodeRouterInput!
    
    var phoneNumber: String?
    
    func configure(with phoneNumber: String) {
        self.phoneNumber = phoneNumber
    }

    func viewIsReady() {
        view.setupInitialState()
        
        if let phoneNumber = phoneNumber {
            view.setPhoneText("\(StringsHelper.string(for: .enterValidationCodeText)) \(phoneNumber)")
        }
    }
    
    func cancelPressed() {
        let tabBarItems = CommonTabBarItem.standardItems(for: nil)
        let tabBarConfiguration = CommonTabBarModuleConfiguration(items: tabBarItems, selectedItem: 2)
        
        router.presentTabBar(with: tabBarConfiguration)
    }
    
    func skipPressed() {
        router.presentCreateAccountModule(with: nil)
    }
    
    func resendPressed() {
        router.presentPreviousController()
    }
    
    func codeEntered(code: String) {
        interactor.validate(code: code, for: phoneNumber!)
    }
    
    // MARK: SignUpEnterCodeInteractorOutput
    
    func didFinishCodeValidation(with result: PhoneValidationServiceResult) {
        switch result {
        case .success:
            router.presentCreateAccountModule(with: phoneNumber)
        case .failure(let error):
            router.presentAlert(with: error.localizedDescription)
        }
    }
}
