import UIKit

class SignUpEnterCodeModuleLoader: BaseModuleLoader {
    
    lazy var storyboardsFactory: StoryboardsFactory = StoryboardsFactoryImplementation()

    override func configureModule(for view: UIViewController) {

        let viewController = view as! SignUpEnterCodeViewController

        let router = SignUpEnterCodeRouter()
        router.createAccountModuleLoader = BaseModuleLoader()
        router.alertsFactory = CommonAlertsFactoryImplementation()
        router.presentationView = view
        router.tabBarModuleLoader = CommonTabBarModuleLoader()

        let presenter = SignUpEnterCodePresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = SignUpEnterCodeInteractor()
        interactor.phoneValidationService = PhoneValidationServiceImplementationMock()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

    override func loadModuleViewController() -> UIViewController {
        return storyboardsFactory.getStoryboard(with: .signUp).instantiateViewController(with: .enterValidationCode)
    }

}
