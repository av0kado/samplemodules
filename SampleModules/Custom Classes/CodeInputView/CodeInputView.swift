import UIKit

class CodeInputView: UIView {
    
    weak var delegate: CodeInputViewDelegate?
    
    fileprivate(set) var code: String = ""
    fileprivate(set) var codeLength: Int = 4
    fileprivate(set) var preferredItemDiameter: CGFloat = 60
    fileprivate(set) var minimumBetweenSpace: CGFloat = 5
    
    // MARK: - Initializers
    
    private init() {
        super.init(frame: CGRect.zero)
        configure()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configure()
    }
    
    convenience init(codeLength: Int = 4, preferredItemDiameter: CGFloat = 60, minimumBetweenSpace: CGFloat = 5) {
        self.init()
        
        self.codeLength = codeLength
        self.preferredItemDiameter = preferredItemDiameter
        self.minimumBetweenSpace = minimumBetweenSpace
        
        configure()
    }
    
    // MARK: - Subviews
    
    private var views: [CodeView]!
    private var spaceViews: [UIView]!
    
    // MARK: - Configuration
    
    private func configure() {
        self.generateViews()
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(handleTap))
        addGestureRecognizer(tapRecognizer)
    }
    // MARK: -
    
    /// Response to tap on view
    @objc private func handleTap() {
        becomeFirstResponder()
    }
    
    /// Generate views due to configuration
    private func generateViews() {
        views = []
        spaceViews = []
        
        for index in 0 ..< codeLength {
            generateView(at: index)
        }
    }
    
    /// Generate `CodeView` at given index
    private func generateView(at index: Int) {
        let codeView = CodeView()
        codeView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(codeView)
        views.append(codeView)
        
        // Horizontal constraints
        if index == 0 {
            NSLayoutConstraint(item: codeView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: 0).isActive = true
        }
        else {
            NSLayoutConstraint(item: codeView, attribute: .leading, relatedBy: .equal, toItem: spaceViews[index-1], attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        }
        
        if index < (codeLength - 1) {
            let spaceView = UIView()
            spaceView.backgroundColor = .clear
            spaceView.translatesAutoresizingMaskIntoConstraints = false
            addSubview(spaceView)
            spaceViews.append(spaceView)
            NSLayoutConstraint(item: spaceView, attribute: .leading, relatedBy: .equal, toItem: codeView, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: spaceView, attribute: .width, relatedBy: .greaterThanOrEqual, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: minimumBetweenSpace).isActive = true
        }
        else {
            NSLayoutConstraint(item: self, attribute: .trailing, relatedBy: .equal, toItem: codeView, attribute: .trailing, multiplier: 1.0, constant: 0).isActive = true
        }
        
        // Equal sizes
        NSLayoutConstraint(item: codeView, attribute: .width, relatedBy: .equal, toItem: codeView, attribute: .height, multiplier: 1.0, constant: 0).isActive = true
        if index > 0 {
            NSLayoutConstraint(item: codeView, attribute: .width, relatedBy: .equal, toItem: views[index - 1], attribute: .width, multiplier: 1.0, constant: 0).isActive = true
        }
        
        // Space views width equality
        if index > 1 {
            NSLayoutConstraint(item: spaceViews[index-1], attribute: .width, relatedBy: .equal, toItem: spaceViews[0], attribute: .width, multiplier: 1.0, constant: 0).isActive = true
        }
        
        // Viertical constraints
        var verticalConstraints = [NSLayoutConstraint]()
        verticalConstraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view" : codeView]))
        if index < (codeLength - 1) {
            verticalConstraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view" : spaceViews[index]]))
        }
        NSLayoutConstraint.activate(verticalConstraints)
        
        // Preferred diameter constraints
        let widthConstraint = NSLayoutConstraint(item: codeView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: preferredItemDiameter)
        widthConstraint.priority = .defaultHigh
        widthConstraint.isActive = true
        
        // Set initial data to view
        codeView.code = ""
    }
}

// MARK: - UIResponder
extension CodeInputView {
    override var canBecomeFirstResponder: Bool {
        return true
    }
}

// MARK: - UITextInputTraits

extension CodeInputView: UITextInputTraits {
    var keyboardType: UIKeyboardType {
        get {
            return .numberPad
        }
        set {
        }
    }
}

// MARK: - UIKeyInput
extension CodeInputView: UIKeyInput {
    
    var hasText: Bool {
        get {
            return code.count > 0
        }
    }
    
    func insertText(_ text: String) {
        do {
            let digitsRegex = try NSRegularExpression(pattern: "[^\\d]", options: .caseInsensitive)
            let digitsString = digitsRegex.stringByReplacingMatches(in: text, options: [], range: NSMakeRange(0, text.count), withTemplate: "")
            
            if code.count >= codeLength || digitsString.count != 1 {
                return
            }
            
            code = code + digitsString
            
            let index: Int = code.count - 1
            views[index].code = digitsString
            
            delegate?.codeInputView(self, didEnter: code)
        }
        catch {
            return
        }
    }
    
    func deleteBackward() {
        if code.count == 0 {
            return
        }
        
        let finalIndex = code.endIndex
        let newFinalIndex = code.index(before: finalIndex)
        code = String(code[code.startIndex ..< newFinalIndex])
        
        let index: Int = code.count
        views[index].code = nil
        
        delegate?.codeInputView(self, didEnter: code)
    }
}

// MARK: - CodeInputViewDelegate
protocol CodeInputViewDelegate: class {
    func codeInputView(_ view: CodeInputView, didEnter code:String)
}

// MARK: - Nested class, CodeView
extension CodeInputView {
    private class CodeView: UIView {
        var code: String? {
            didSet {
                self.updateForNewCode()
            }
        }
        
        private var label: UILabel
        
        // MARK: - Initializers
        
        init() {
            self.label = UILabel()
            super.init(frame: .zero)
            self.configure()
        }
        
        override init(frame: CGRect) {
            self.label = UILabel(frame: frame)
            super.init(frame: frame)
            self.configure()
        }
        
        required init?(coder aDecoder: NSCoder) {
            self.label = UILabel(frame: .zero)
            super.init(coder: aDecoder)
            self.configure()
        }
        
        func configure() {
            label.font = UIFont.systemFont(ofSize: 42.0)
            label.textColor = UIColor.white
            label.textAlignment = .center
            
            label.translatesAutoresizingMaskIntoConstraints = false
            addSubview(label)
            
            var constraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[view]|", options: [], metrics: nil, views: ["view" : label])
            constraints.append(contentsOf: NSLayoutConstraint.constraints(withVisualFormat: "V:|[view]|", options: [], metrics: nil, views: ["view" : label]))
            
            NSLayoutConstraint.activate(constraints)
            
            cornerRadius = bounds.width / 2
        }
        
        // MARK: -
        
        /// Update appearance for new value
        func updateForNewCode() {
            if let length = code?.count, length > 0 {
                label.text = code
                backgroundColor = UIColor.blue
            } else {
                label.text = ""
                backgroundColor = UIColor(white: 203.0/255.0, alpha: 0.2)
            }
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            
            cornerRadius = bounds.width / 2
            label.font = label.font.withSize(bounds.width * 7 / 10)
        }
    }
}
