import Foundation

enum PhotosAuthorizationResult {
    case success
    case failure
}

/// Authorizes user with Photo framework
protocol PhotosAuthorizationManager {
    
    /// Request user for authorization to use photos
    func requestAuthorization(with completion: @escaping (PhotosAuthorizationResult) -> Void)
}
