import Photos

class PhotosAuthorizationManagerImplementation: PhotosAuthorizationManager {
    
    func requestAuthorization(with completion: @escaping (PhotosAuthorizationResult) -> Void) {
        
        let status = PHPhotoLibrary.authorizationStatus()
        
        switch status {
        case .denied:
            fallthrough
        case .restricted:
            completion(.failure)
        case .authorized:
            completion(.success)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({ (status) in
                
                let success = (status == .authorized)
                
                if success {
                    completion(.success)
                }
                else {
                    completion(.failure)
                }
            })
        }
    }
}
