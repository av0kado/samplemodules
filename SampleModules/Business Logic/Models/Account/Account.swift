
/// Account type
enum AccountType: String {
    case typical    = "typical"
    case other      = "other"
}

protocol Account {
    
    var id: Int? { get set }
    var userName: String? { get set }
    var displayName: String? { get set }
    var accountType: AccountType? { get set }
}
