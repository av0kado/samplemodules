enum PhoneValidationServiceResult {
    case success()
    case failure(Error)
}

import Foundation

protocol PhoneValidationService {
    
    /// Requests server to send SMS to user
    ///
    /// - Parameter phoneNumber: phone number
    /// - Returns: result of request
    func requestValidation(for phoneNumber: String, completionHandler: @escaping (PhoneValidationServiceResult) -> Void) -> URLSessionTask?
    
    /// Requests server to validate phone number with entered code
    ///
    /// - Parameters:
    ///   - phoneNumber: phone number passing validation
    ///   - code: entered validation code
    /// - Returns: result of request
    func validatePhoneNumber(_ phoneNumber: String, with code: String, completionHandler: @escaping (PhoneValidationServiceResult) -> Void) -> URLSessionTask?
}
