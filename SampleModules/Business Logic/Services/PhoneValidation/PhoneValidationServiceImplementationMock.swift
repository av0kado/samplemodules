import Foundation

class PhoneValidationServiceImplementationMock: PhoneValidationService {
    
    // MARK: - PhoneValidationService
    
    func requestValidation(for phoneNumber: String, completionHandler: @escaping (PhoneValidationServiceResult) -> Void) -> URLSessionTask? {
        
        completionHandler(.success())
        return nil
    }
    
    func validatePhoneNumber(_ phoneNumber: String, with code: String, completionHandler: @escaping (PhoneValidationServiceResult) -> Void) -> URLSessionTask? {
        
        completionHandler(.success())
        return nil
    }
}
