//
//  ViewController.swift
//  SampleModules
//
//  Created by Amir Zigangarayev on 18/01/2018.
//  Copyright © 2018 Amir Zigangarayev. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ImagePickerEventHandler {

    @IBOutlet weak var imageView: UIImageView!
    
    lazy var photosAuthorizationManager: PhotosAuthorizationManager! = PhotosAuthorizationManagerImplementation()
    lazy var alertsFactory: CommonAlertsFactory! = CommonAlertsFactoryImplementation()
    
    lazy var imagesModuleLoader: BaseModuleLoader! = ImagePickerModuleLoader()
    lazy var enterCodeModuleLoader: BaseModuleLoader! = SignUpEnterCodeModuleLoader()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func selectImageButtonPressed() {
        
        photosAuthorizationManager.requestAuthorization { (result) in
            switch result {
            case .success:
                let controller = self.imagesModuleLoader.loadModuleViewController()
                
                if let configurableController = controller as? ConfigurableModuleController {
                    configurableController.configureModule(with: (self as ImagePickerEventHandler))
                }
                
                self.present(controller, animated: true, completion: nil)
            case .failure:
                let controller = self.alertsFactory.getOKAlert(withTitle: "Can't open :(", text: nil)
                self.present(controller, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func goToPhoneButtonPressed() {
        let phoneNumber = String(arc4random() % 100000)
        let controller = enterCodeModuleLoader.loadModuleViewController()
        
        if let configurableController = controller as? ConfigurableModuleController {
            configurableController.configureModule(with: phoneNumber)
        }
        
        UIApplication.appDelegate.setNewRootViewController(controller)
    }
    
    // MARK: - ImagePickerEventHandler
    
    func imagePickerDidFinish(with image: UIImage) {
        imageView.image = image
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerDidCancel() {
        self.dismiss(animated: true, completion: nil)
    }
}

