import UIKit

class CommonAlertsFactoryImplementation: CommonAlertsFactory {
    
    func getOKAlert(withTitle title: String?, text: String?) -> UIViewController {
        return getOKAlert(withTitle: title, text: text, completionBlock: nil)
    }
    
    func getOKAlert(withTitle title: String?, text: String?, completionBlock: AlertsFactoryCompletion?) -> UIViewController {
        let alertController = UIAlertController(title: title, message: text, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .cancel) { (_) in
            completionBlock?()
        }
        alertController.addAction(okAction)
        
        return alertController
    }
}
