import UIKit

typealias AlertsFactoryCompletion = () -> Void

/// Produces UIAlertController alerts with `OK` button
protocol CommonAlertsFactory {
    
    /// Create UIAlertController with given title and text
    func getOKAlert(withTitle title: String?, text: String?) -> UIViewController
    
    /// Create UIAlertController with given title, text and completionBlock
    func getOKAlert(withTitle title: String?, text: String?, completionBlock: AlertsFactoryCompletion?) -> UIViewController
}
