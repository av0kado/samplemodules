//
//  StoryboardsFactory.swift
//  Portable
//

import UIKit

/// Factory for generating storyboards
protocol StoryboardsFactory {
    func getStoryboard(with name: StoryboardName) -> UIStoryboard
}

/// Storyboard identifiers
///
/// - main: Main story
/// - imagePicker: ImagePicker story
/// - signUp: Sign Up story
enum StoryboardName: String {
    case main                       = "Main"
    case imagePicker                = "ImagePicker"
    case signUp                     = "SignUp"
    case commonTabBar               = "CommonTabBar"
}

/// ModuleName identifiers / Storyboard Identifiers of modules
///
/// - imagePickerCollection: ImagePicker module
enum ModuleName: String {
    case imagePickerCollection      = "ImagePickerCollectionView"
    case enterValidationCode        = "EnterValidationCode"
    case tabBarController           = "TabBarController"
}

extension UIStoryboard {
    
    /// Instantiates module view controller for given moduleName
    ///
    /// - Parameter moduleName: module to be loaded
    /// - Returns: instantiated UIViewController
    func instantiateViewController(with moduleName: ModuleName) -> UIViewController {
        return instantiateViewController(withIdentifier: moduleName.rawValue)
    }
}
