//
//  StringsHelper.swift
//  Portable
//

class StringsHelper {
    
    enum StringKey: String {
        
        // Sample Text
        case sample                             = "Sample"
        
        // Text to tell user to validate phone number, sign up -> enter validation code
        case enterValidationCodeText            = "Please enter the verification code sent to"
    }
    
    static func string(for key: StringKey) -> String {
        switch key {
        default:
            return key.rawValue
        }
    }
}
